# Panel Data Regression

## Description

Along with a team of 4 other analysts, we have used wage and industrial goods datasets to understand regression analysis with panel data. It was important to treat panel data differently than normal datasets to overcome spurious results.

We have used fixed effects, random effects and 2 stage least square methods to model panel data.